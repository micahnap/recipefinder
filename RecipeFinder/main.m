//
//  main.m
//  RecipeFinder
//
//  Created by Micah Napier on 30/10/2014.
//  Copyright (c) 2014 code challenge. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
