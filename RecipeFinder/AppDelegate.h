//
//  AppDelegate.h
//  RecipeFinder
//
//  Created by Micah Napier on 30/10/2014.
//  Copyright (c) 2014 code challenge. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

